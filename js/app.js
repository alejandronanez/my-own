$(document).ready(function() {
	'use strict';

	// Initialize TASKMANAGER
	var taskManager = new TASKMANAGER.Task('task-manager'),
		taskmanagerForm = $('#taskmanager-form'),
		taskName = '';

	// Listen to click event and then add the task to the list
	taskmanagerForm.on('submit', function (e) {
		e.preventDefault();
		// Add the task to the list
		taskManager.addTask();
	});

});