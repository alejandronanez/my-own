'use strict';
var TASKMANAGER = TASKMANAGER || {};

TASKMANAGER.Task = function (element) {
    this.element = $('#' + element);
    this.taskInputElement = this.element.find('input[type="text"]');
    this.taskList = []; // Where to store all the tasks
};

TASKMANAGER.Task.prototype = function () {

    var htmlTemplate = '',
        htmlRenderedTemplate = '',
        JSONData = {},
        /**
        * Add a task into the list
        * @param {String} task The task to add to the list
        */
        addTask = function () {
            // Add another task to the list
            this.taskList.push(this.taskInputElement.val());
            JSONData.allTasks = this.taskList;
            // Clean the input field
            cleanField.call(this);
            // Show the new task in the template
            renderListElements.call(this);
        },
        /**
         * Delete the content of the input
         * @return The input field inner text deleted.
         */
        cleanField = function () {
            this.taskInputElement.val('');
        },
        /**
         * Get all the tasks stored
         * @return {Object} Tasks in an Object format
         */
        getAllTasks = function () {
            return JSONData;
        },
        /**
         * Render the list elements on the taskLits
         * @return Each task render in the DOM
         */
        renderListElements = function () {
            // The HTML Template
            htmlTemplate = $('#taskListTemplate').html();
            // HTML Rendered Template
            htmlRenderedTemplate = Mustache.render(htmlTemplate, JSONData);
            $('#taskList').html( htmlRenderedTemplate );
        };

    // Public API
    return {
        addTask: addTask
    };
}();